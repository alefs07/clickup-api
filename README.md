# ClickUp API

API that posts messages to ClickUp tasks.

## Endpoints

| Endpoint | Method | Header | Body | Query Params | Response |
|:-:|:-:|:-:|:-:|:-:|:-:|
|`/comments`|`GET`|`"Content-Type": "application/json"`|/|`username="<discord_tag>", task_id="<task_id>"`|`"name": "<task_name>","url": "<task_url>"`|
|`/comments`|`POST`|`'Content-Type': 'application/json'`|`"username": "<discord_tag>","comment": "<comment>"`|`"task_id":"<task_id>"`|`"name":"<task_name>"`|
