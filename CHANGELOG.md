# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.1] - unreleased

- 🐛 remove urls of concatenated attached files in the message before sending to ClickUp

## [1.2.0] - Jan 23, 2023

### Added

- ✨ add concatenation of the urls of the images attached to the message posted by discord to be published in the ClickUp
- ✨ attach the files sent in the Discord message to the ClickUp task

## [1.1.0] - Jan 16, 2023

### Added

- 🐛 add replacing ids with usernames
- ♻️ add new dashboard to keep the mappings between discord tag and clickup token and between user discord id and clickup username

## [1.0.0] - Jan 13, 2023

### Added

- ✨ initial release
